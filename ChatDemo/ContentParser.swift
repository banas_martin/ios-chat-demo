//
//  ContentParser.swift
//  ChatDemo
//
//  Created by Banas Martin on 10/08/16.
//  Copyright © 2016 Banas. All rights reserved.
//

import Foundation

class ContentParser {

    private let mentionsExpresion = "(?:^@|\\s@)([a-zA-Z0-9]{1}[a-zA-Z0-9\\-\\_\\.]+)"
    private let emoticonsExpresion = "\\(([a-zA-Z0-9]{1,15})\\)"

    private let linkResolver: LinkMetaDataResolving

    init(linkResolver: LinkMetaDataResolving) {
        self.linkResolver = linkResolver
    }

    /// Returns a json string containing array of emoticons, mentions and links (with fetched titles)
    /// In case of error the json string could be still returned
    ///
    /// e.g. {\"links\":[{\"link\":\"https:\/\/twitter.com\",\"title\":\"Website title"}],\"mentions\":[\"bob\",\"john\"],\"emoticons\":[\"success\"]}"
    func process(string: String, completion: ((content: String?, error: NSError?) -> Void))  {
        let mentions = processMentions(string)
        let emoticons = processEmoticons(string)
        let links = processURLLinks(string)

        linkResolver.resolve(links) { (metaData, error) in
            var content: [String: [AnyObject]] = ["mentions": mentions, "emoticons": emoticons]

            /// In case when metaData are empty
            if metaData.isEmpty {

                /// Process array of current links
                content["links"] = links

            /// Build the dictionary with all elements
            } else {
                var links = [[String: AnyObject]]()

                for meta in metaData {
                    var item: [String : AnyObject] = ["link": meta.url.absoluteString]

                    if let title = meta.title {
                        item["title"] = title
                    }

                    links.append(item)
                }

                content["links"] = links
            }

            do {
                let jsonData = try NSJSONSerialization.dataWithJSONObject(content, options: []) // We don't want to format the output as pretty printed (.PrettyPrinted)
                let jsonString = String(data: jsonData, encoding: NSUTF8StringEncoding)
                completion(content: jsonString, error: error)

            } catch let error as NSError {
                completion(content: nil, error: error)
            }
        }
    }

    /// Returns an array of parsed mentions from input string '@jon and @peter' -> [jon, peter]
    func processMentions(string: String) -> [String] {
        var mentions = [String]()

        do {
            let regex = try NSRegularExpression(pattern: mentionsExpresion, options: [.CaseInsensitive])

            let matches = regex.matchesInString(string, options: [.WithoutAnchoringBounds], range: NSRange(location: 0, length: string.characters.count))

            let trimCharacters = NSMutableCharacterSet.whitespaceAndNewlineCharacterSet()
            trimCharacters.formUnionWithCharacterSet(NSCharacterSet(charactersInString: "@@"))

            for match in matches {
                var item = (string as NSString).substringWithRange(match.range)
                item = item.stringByTrimmingCharactersInSet(trimCharacters)
                mentions.append(item)
            }

        } catch let error as NSError {
            print(error.description)
        }

        return mentions
    }

    /// Returns an array of parsed emoticons from input string '(coffee) and (drink)' -> [coffee, drink]
    func processEmoticons(string: String) -> [String] {
        var emoticons = [String]()

        do {
            let regex = try NSRegularExpression(pattern: emoticonsExpresion, options: [.CaseInsensitive])

            let matches = regex.matchesInString(string, options: [.WithoutAnchoringBounds], range: NSRange(location: 0, length: string.characters.count))

            let trimCharacters = NSMutableCharacterSet.whitespaceAndNewlineCharacterSet()
            trimCharacters.formUnionWithCharacterSet(NSCharacterSet(charactersInString: "()"))

            for match in matches {
                var item = (string as NSString).substringWithRange(match.range)
                item = item.stringByTrimmingCharactersInSet(trimCharacters)
                emoticons.append(item)
            }

        } catch let error as NSError {
            print(error.description)
        }

        return emoticons
    }

    /// Returns an array of parsed urls from input string 'www.google.com' -> [www.google.com]
    func processURLLinks(string: String) -> [String] {
        var urls = [String]()

        do {
            let detector = try NSDataDetector(types: NSTextCheckingType.Link.rawValue)

            let matches = detector.matchesInString(string, options: [.WithoutAnchoringBounds], range: NSRange(location: 0, length: string.characters.count))

            for match in matches {
                let item = (string as NSString).substringWithRange(match.range)
                urls.append(item)
            }

        } catch let error as NSError {
            print(error.description)
        }

        return urls
    }
}
