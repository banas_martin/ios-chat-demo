//
//  LinkMetaDataResolver.swift
//  ChatDemo
//
//  Created by Banas Martin on 12/08/16.
//  Copyright © 2016 Banas. All rights reserved.
//

import UIKit

class LinkMetaDataResolver: LinkMetaDataResolving {

    private let requestTimeout: NSTimeInterval = 30
    private let networking: URLSessionProtocol

    init(networking: URLSessionProtocol) {
        self.networking = networking
    }

    /// Returns an array of processed links meta data from a link paths
    /// Returns an error in case of networking issue
    /// The link path is cast to NSURL. In case when the link path is not valid NSURL object the link is ignored
    ///
    /// Completion on a main thread
    func resolve(links: [String], completion: ((metaData: [LinkMetaData], error: NSError?) -> Void)) {
        var urls = [NSURL]()

        for link in links {
            if let url = NSURL(string: link) {
                urls.append(url)

            } else {
                print("Invalid url: \(link)")
            }
        }

        if urls.isEmpty {
            print("links dont contain any valid urls - returning")
            completion(metaData: [], error: nil)

        } else {
            resolveMetaData(urls, completion: { (metaData, error) in
                dispatch_async(dispatch_get_main_queue()) {
                    completion(metaData: metaData, error:  error)
                }
            })
        }
    }

    private func resolveMetaData(urls: [NSURL], completion: ((metaData: [LinkMetaData], error: NSError?) -> Void)) {
        var taskError: NSError?
        var metaData = [LinkMetaData]()

        for url in urls {
            fetchHTML(url, completion: { (html, error) in
                if taskError == nil {
                    taskError = error
                }

                let meta = LinkMetaData(url: url, html: html)
                metaData.append(meta)

                if metaData.count >= urls.count {
                    completion(metaData: metaData, error: taskError)
                }
            })
        }
    }

    private func fetchHTML(url: NSURL, completion: ((html: String?, error: NSError?) -> Void)) {
        let request = NSMutableURLRequest(URL: url, cachePolicy: .ReturnCacheDataElseLoad, timeoutInterval: requestTimeout)

        networking.dataTaskWithRequest(request) { (data, response, error) in
            var html: String?

            if let data = data {
                html = String(data: data, encoding: NSUTF8StringEncoding)
            }

            completion(html: html, error: error)
            
        }.resume()
    }
}
