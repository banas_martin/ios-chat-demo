//
//  ChatModel.swift
//  ChatDemo
//
//  Created by Banas Martin on 15/08/16.
//  Copyright © 2016 Banas. All rights reserved.
//

import Foundation

class ChatModel {

    var parserHandler: ((content: String) -> Void)?
    var errorHandler: ((message: String) -> Void)?

    private let contentParser: ContentParser

    init() {
        let networking = NSURLSession.sharedSession()
        let linkResolver = LinkMetaDataResolver(networking: networking)
        contentParser = ContentParser(linkResolver: linkResolver)
    }

    func parseContent(string: String) {
        contentParser.process(string) { [weak self](content, error) in
            if let content = content {
                self?.parserHandler?(content: content)

            } else if let error = error {
                self?.errorHandler?(message: error.localizedDescription)
            }
        }
    }
}
