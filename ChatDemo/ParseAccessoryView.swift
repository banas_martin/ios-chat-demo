//
//  AccessoryView.swift
//  ChatDemo
//
//  Created by Banas Martin on 15/08/16.
//  Copyright © 2016 Banas. All rights reserved.
//

import UIKit

class ParseAccessoryView: UIView {

    private var action: (() -> Void)!

    convenience init(size: CGFloat, action: (() -> Void)) {
        self.init(frame: CGRectMake(0, 0, 0, 54))

        self.action = action

        translatesAutoresizingMaskIntoConstraints = false

        let parseButton = UIButton(type: .Custom)
        parseButton.backgroundColor = UIColor.blueColor()
        parseButton.setTitle(NSLocalizedString("parse-accessory-view-title", value: "Parse", comment: "Button's title - Parse provided content"), forState: .Normal)
        parseButton.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        parseButton.addTarget(self, action: #selector(parse(_:)), forControlEvents: .TouchUpInside)
        parseButton.setBackgroundImage(UIImage.imageWithColor(UIColor.lightGrayColor()), forState: .Normal)
        parseButton.setBackgroundImage(UIImage.imageWithColor(UIColor.darkGrayColor()), forState: .Highlighted)

        parseButton.translatesAutoresizingMaskIntoConstraints = false
        addSubview(parseButton)

        let views = ["parseButton": parseButton]

        addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-(0)-[parseButton]-(0)-|", options: [], metrics: nil, views: views))
        addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|-(0)-[parseButton]-(0)-|", options: [], metrics: nil, views: views))
    }

    @objc func parse(sender: UIButton) {
        action()
    }
}
