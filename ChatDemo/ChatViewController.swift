//
//  ViewController.swift
//  ChatDemo
//
//  Created by Banas Martin on 10/08/16.
//  Copyright © 2016 Banas. All rights reserved.
//

import UIKit

class ChatViewController: UIViewController {

    @IBOutlet weak var textView: UITextView!

    override var inputAccessoryView: ParseAccessoryView {
        return accessoryView
    }

    private let model = ChatModel()
    private var accessoryView: ParseAccessoryView!

    override func viewDidLoad() {
        super.viewDidLoad()

        title = NSLocalizedString("chat-navigation-title", value: "Chat Demo", comment: "Navigation's title")

        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .Refresh, target: self, action: #selector(refresh(_:)))

        accessoryView = ParseAccessoryView(size: 54, action: { [unowned self] in
            self.startParsing(self.textView.text)
        })

        model.parserHandler = { [weak self] (content) in
            self?.finishParsing(content)
        }

        model.errorHandler = { [weak self] (message) in
            self?.finishParsing(message)
        }

        reload()
    }

    override func canBecomeFirstResponder() -> Bool {
        return true
    }

    // MARK: Private methods

    private func reload() {
        textView.text = "@bob @john (success) such a cool feature; https://twitter.com/jdorfman/status/430511497475670016"
    }

    private func startParsing(string: String) {
        textView.resignFirstResponder()

        textView.userInteractionEnabled = false
        accessoryView.userInteractionEnabled = false
        model.parseContent(string)
    }

    private func finishParsing(string: String) {
        textView.text = string
        accessoryView.userInteractionEnabled = true
        textView.userInteractionEnabled = true
    }

    @objc private func refresh(sender: UIBarButtonItem) {
        textView.resignFirstResponder()
        reload()
    }
}

extension ChatViewController: UITextViewDelegate {

    func textViewDidBeginEditing(textView: UITextView) {
        textView.text = ""
    }
}

