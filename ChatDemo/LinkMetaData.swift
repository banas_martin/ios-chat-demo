//
//  LinkMetaData.swift
//  ChatDemo
//
//  Created by Banas Martin on 12/08/16.
//  Copyright © 2016 Banas. All rights reserved.
//

import UIKit

struct LinkMetaData {

    let url: NSURL
    var title: String?
    var image: UIImage? // Currently not used

    private let titleTagExpresion = "<title.*?>\\s*(.*?)\\s*<\\/title>"

    init(url: NSURL, html: String?) {
        self.url = url

        if let html = html {
            parseHTML(html)
        }
    }

    /// Title parsing doesn't cover all cases. There is a nice post which briefly describing the issue http://bit.ly/2aOypjz
    /// Also it doesn't support Open Graph protocol which could be a nice enhancement
    private mutating func parseHTML(html: String) {

        // Parse title
        do {
            let regex = try NSRegularExpression(pattern: titleTagExpresion, options: [.CaseInsensitive])

            /// Find only first match - we don't expect to have more titles on the website
            if let result = regex.firstMatchInString(html, options: [.WithoutAnchoringBounds], range: NSRange(location: 0, length: html.characters.count)) {
                var item = (html as NSString).substringWithRange(result.range)
                item = item.stringByReplacingOccurrencesOfString("<title>", withString: "", options: [.CaseInsensitiveSearch], range: nil)
                item = item.stringByReplacingOccurrencesOfString("</title>", withString: "", options: [.CaseInsensitiveSearch], range: nil)

                title = item
            }

        } catch let error as NSError {
            print(error.description)
        }
    }
}