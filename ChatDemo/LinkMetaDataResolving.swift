//
//  LinkMetaDataResolving.swift
//  ChatDemo
//
//  Created by Banas Martin on 12/08/16.
//  Copyright © 2016 Banas. All rights reserved.
//

import Foundation

protocol LinkMetaDataResolving: class {

    func resolve(links: [String], completion: ((metaData: [LinkMetaData], error: NSError?) -> Void))
}
