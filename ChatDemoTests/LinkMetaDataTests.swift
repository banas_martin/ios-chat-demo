//
//  LinkMetaDataTests.swift
//  ChatDemo
//
//  Created by Banas Martin on 12/08/16.
//  Copyright © 2016 Banas. All rights reserved.
//

import XCTest
@testable import ChatDemo

class LinkMetaDataTests: XCTestCase {

    override func setUp() {
        super.setUp()
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func test_title_parsing_true() {

        // Prepare
        let title = "Twitter"
        let link = "https://twitter.com/jdorfman/status/430511497475670016"
        let url = NSURL(string: link)!
        let html = "<html><head><title>\(title)</title></head></html>"

        // Test
        let metaData = LinkMetaData(url: url, html: html)

        // Verify
        XCTAssertTrue(title == metaData.title, "parsed title is different")
    }

    func test_title_parsing_case_insensitive_true() {

        // Prepare
        let title = "Twitter"
        let link = "https://twitter.com/jdorfman/status/430511497475670016"
        let url = NSURL(string: link)!
        let html = "<html><head><TITLE>\(title)</TITLE></head></html>"

        // Test
        let metaData = LinkMetaData(url: url, html: html)

        // Verify
        XCTAssertTrue(title == metaData.title, "parsed title is different")
    }

    func test_title_parsing_false() {

        // Prepare
        let title = "Twitter"
        let link = "https://twitter.com/jdorfman/status/430511497475670016"
        let url = NSURL(string: link)!
        let html = "<html><head foo=\"<title>\"><title>\(title)</title></head></html>"

        // Test
        let metaData = LinkMetaData(url: url, html: html)

        // Verify
        XCTAssertFalse(title == metaData.title, "parsed title is different")
    }

    func test_title_parsing_comments_false() {

        // Prepare
        let title = "Twitter"
        let link = "https://twitter.com/jdorfman/status/430511497475670016"
        let url = NSURL(string: link)!
        let html = "<html><head><!-- <title>old</title> --><title>\(title)</title></head></html>"

        // Test
        let metaData = LinkMetaData(url: url, html: html)

        // Verify
        XCTAssertFalse(title == metaData.title, "parsed title is different")
    }
}
