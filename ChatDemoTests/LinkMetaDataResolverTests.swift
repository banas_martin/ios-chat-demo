//
//  LinkMetaDataResolverTests.swift
//  ChatDemo
//
//  Created by Banas Martin on 12/08/16.
//  Copyright © 2016 Banas. All rights reserved.
//

import XCTest
@testable import ChatDemo

class LinkMetaDataResolverTests: XCTestCase {

    var linkResolver: LinkMetaDataResolving!
    var networking: URLSessionMock!

    override func setUp() {
        super.setUp()

        networking = URLSessionMock()
        linkResolver = LinkMetaDataResolver(networking: networking)
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func test_resolve_link_true() {

        // Prepare
        let title = "Justin Dorfman on Twitter: &quot;nice @littlebigdetail from @HipChat (shows hex colors when pasted in chat). http://t.co/7cI6Gjy5pq&quot;"
        let link = "https://twitter.com/jdorfman/status/430511497475670016"
        let url = NSURL(string: link)!
        let response = TaskResponseMock(requestURL: url, delay: 1)
        response.data = "<html><head><title>\(title)</title></head></html>".dataUsingEncoding(NSUTF8StringEncoding)
        networking.reponseMocks.append(response)

        let expectation = expectationWithDescription("expectations")

        // Test
        linkResolver.resolve([link]) { (metaData, error) in

            // Verify
            XCTAssertNil(error, "error occurred in happy scenario")

            if let meta = metaData.first {
                XCTAssertTrue(meta.url.absoluteString == link, "parsed url isn't equal to original link")
                XCTAssertTrue(meta.title == title, "missing title for the website")
            }
            
            expectation.fulfill()
        }

        waitForExpectationsWithTimeout(10) { (error: NSError?) -> Void in
            guard error == nil else {
                XCTFail("The request took too long to be processed")
                return
            }
        }
    }

    func test_resolve_link_false() {

        // Prepare
        let title = "Justin Dorfman on Twitter: &quot;nice @littlebigdetail from @HipChat (shows hex colors when pasted in chat). http://t.co/7cI6Gjy5pq&quot;"
        let link = "https://twitter.com/jdorfman/status/430511497475670016"
        let url = NSURL(string: link)!
        let response = TaskResponseMock(requestURL: url, delay: 1)
        response.data = "<html><head><title>\(title)</title></head></html>".dataUsingEncoding(NSUTF8StringEncoding)
        response.error = NSError(domain: "error", code: 400, userInfo: nil)
        networking.reponseMocks.append(response)

        let expectation = expectationWithDescription("expectations")

        // Test
        linkResolver.resolve([link]) { (metaData, error) in

            // Verify
            XCTAssertNotNil(error, "resolved should complete with error")

            expectation.fulfill()
        }

        waitForExpectationsWithTimeout(10) { (error: NSError?) -> Void in
            guard error == nil else {
                XCTFail("The request took too long to be processed")
                return
            }
        }
    }
}
