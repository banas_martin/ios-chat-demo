//
//  TaskResponseMock.swift
//  ChatDemo
//
//  Created by Banas Martin on 12/08/16.
//  Copyright © 2016 Banas. All rights reserved.
//

import Foundation

class TaskResponseMock {
    let requestURL: NSURL
    let delay: NSTimeInterval

    var data: NSData?
    var URL: NSURL?
    var response: NSURLResponse?
    var error: NSError?

    init(requestURL: NSURL, delay: NSTimeInterval = 0) {
        self.requestURL = requestURL
        self.delay = delay
    }
}