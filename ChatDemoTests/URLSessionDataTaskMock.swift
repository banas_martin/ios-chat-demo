//
//  URLSessionDataTaskMock.swift
//  ChatDemo
//
//  Created by Banas Martin on 12/08/16.
//  Copyright © 2016 Banas. All rights reserved.
//

import Foundation
@testable import ChatDemo

class URLSessionDataTaskMock: URLSessionDataTaskProtocol {

    private (set) var resumeWasCalled = false

    func resume() {
        resumeWasCalled = true
    }
}
