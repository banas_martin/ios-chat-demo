//
//  ChatDemoTests.swift
//  ChatDemoTests
//
//  Created by Banas Martin on 10/08/16.
//  Copyright © 2016 Banas. All rights reserved.
//

import XCTest
@testable import ChatDemo

class ContentParserTests: XCTestCase {

    var networking: URLSessionMock!
    var parser: ContentParser!

    override func setUp() {
        super.setUp()

        networking = URLSessionMock()
        let linkResolver = LinkMetaDataResolver(networking: networking)
        parser = ContentParser(linkResolver: linkResolver)
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func test_process_whole_string() {

        // Prepare
        let title = "Justin Dorfman on Twitter: &quot;nice @littlebigdetail from @HipChat (shows hex colors when pasted in chat). http://t.co/7cI6Gjy5pq&quot;"
        let link = "https://twitter.com/jdorfman/status/430511497475670016"
        let url = NSURL(string: link)!
        let response = TaskResponseMock(requestURL: url, delay: 1)
        response.data = "<html><head><title>\(title)</title></head></html>".dataUsingEncoding(NSUTF8StringEncoding)
        networking.reponseMocks.append(response)

        let expectation = expectationWithDescription("expectations")
        
        // Test
        parser.process("@bob @john (success) such a cool feature; https://twitter.com/jdorfman/status/430511497475670016") { (content, error) in

            // Verify
            XCTAssertTrue(error == nil)
            XCTAssertTrue(content?.containsString("{\"links\":[{\"link\":\"https:\\/\\/twitter.com\\/jdorfman\\/status\\/430511497475670016\",\"title\":\"Justin Dorfman on Twitter: &quot;nice @littlebigdetail from @HipChat (shows hex colors when pasted in chat). http:\\/\\/t.co\\/7cI6Gjy5pq&quot;\"}],\"mentions\":[\"bob\",\"john\"],\"emoticons\":[\"success\"]}") == true, "doesn't contain expected output")

            expectation.fulfill()
        }

        waitForExpectationsWithTimeout(10) { (error: NSError?) -> Void in
            guard error == nil else {
                XCTFail("The request took too long to be processed")
                return
            }
        }
    }
    
    func test_process_mentions() {

        let shouldContain = ["chris", "peter", "me"]

        // Test
        let mentions = parser.processMentions("@chris @peter you around @me@fail")

        // Verify
        XCTAssertTrue(mentions == shouldContain, "doesn't contain expected output")
    }

    func test_process_emoticons() {

        let shouldContain = ["megusta", "coffee", "pen", "blob"]

        // Test
        let emoticons = parser.processEmoticons("(megusta) Good morning! (coffee)inside(pen)string(star(blob))")

        // Verify
        XCTAssertTrue(emoticons == shouldContain, "doesn't contain expected output")
    }

    func test_process_links() {

        let shouldContain = ["https://twitter.com/jdorfman/status/430511497475670016", "www.google.com", "http://amazon.com/status/123898123812;#212je21"]

        // Test
        let links = parser.processURLLinks("this is https://twitter.com/jdorfman/status/430511497475670016 link and there is www.google.com and another one http://amazon.com/status/123898123812;#212je21")

        // Verify
        XCTAssertTrue(links == shouldContain, "doesn't contain expected output")
    }
}
